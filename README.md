morphtree
=========

  * A generic [morphdom](https://github.com/patrick-steele-idem/morphdom) algorithm implementation for golang.

SYNOPSIS
--------

```golang
package main

import (
  _ "github.com/nyarla/morphtree/backend/golang"  // use pure-golang tree backend implementation
  . "github.com/nyarla/morphtree/dsl"             // use dsl
  "github.com/nyarla/morphtree/morph"
  "github.com/nyarla/morphtree/tree"
)

func main() {
  tree := H(`p#msg`, T(`hello, world!`)).Describe(tree.CreateElement(`p`))
  patch := H(`p#msg`, T(hi)).Describe(tree.CreateElement(`p`))
  
  node := morph.New().Morph(tree, patch, false)
}
```

DOCUMENTATION
-------------

See: <https://godoc.org/github.com/nyarla/morphtree/>

COPYRIGHT
---------

This library uses [morphdom](https://github.com/patrick-steele-idem/morphdom) algorithm,
and which is based on [morphdom](https://github.com/patrick-steele-idem/morphdom) source code.

[morphdom](https://github.com/patrick-steele-idem/morphdom) is implemented by [patrick-steele-idem](https://github.com/patrick-steele-idem),
and which is under the [MIT-license](https://github.com/patrick-steele-idem/morphdom/blob/master/LICENSE).

And, implementation of `backend/golang` and `renderer/html` are referenced at `golang.org/x/net/html`,
which is under [3-Clause BSD License.](https://github.com/golang/net/blob/master/LICENSE).

LICENSE
-------

This library is under the [MIT-license](https://github.com/nyarla/morphtree/LICENSE.md)

AUTHOR
------

Naoki OKAMURA a.k.a nyarla <nyarla@thotep.net>
