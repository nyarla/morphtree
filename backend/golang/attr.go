package golang

type attribute struct {
	namespace, prefix, name, key, value string
}

func (a *attribute) Namespace() string {
	if a != nil {
		return a.namespace
	}

	return ``
}

func (a *attribute) Prefix() string {
	if a != nil {
		return a.prefix
	}

	return ``
}

func (a *attribute) Name() string {
	if a != nil {
		return a.name
	}

	return ``
}

func (a *attribute) Key() string {
	if a != nil {
		return a.key
	}

	return ``
}

func (a *attribute) Value() string {
	if a != nil {
		return a.value
	}

	return ``
}
