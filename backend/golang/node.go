package golang

import (
	"strings"

	"github.com/nyarla/morphtree/morph"
	"github.com/nyarla/morphtree/tree"
)

type node struct {
	namespace string
	kind      tree.NodeType
	prefix    string
	key       string
	data      string

	firstChild, lastChild, nextSibling, prevSibling, parent *node

	events map[string]func(target tree.Node) tree.Node

	attrs []*attribute
}

func (n *node) Namespace() string {
	if n != nil {
		return n.namespace
	}

	return ``
}

func (n *node) Type() tree.NodeType {
	if n != nil {
		return n.kind
	}

	return tree.UnknownNode
}

func (n *node) Prefix() string {
	if n != nil {
		return n.prefix
	}

	return ``
}

func (n *node) Name() string {
	if n == nil {
		return ``
	}

	switch n.kind {
	case tree.TextNode:
		return `#text`
	case tree.CommentNode:
		return `#comment`
	case tree.ElementNode:
		return n.data
	}

	return ``
}

func (n *node) Key() string {
	if n == nil {
		return ``
	}

	if n.key == `` {
		if id := n.GetAttr(`id`); id != nil && id.Value() != `` {
			n.key = id.Value()
		}
	}

	return n.key
}

func (n *node) FirstChild() tree.Node {
	if n != nil && n.firstChild != nil {
		return n.firstChild
	}

	return nil
}

func (n *node) LastChild() tree.Node {
	if n != nil && n.lastChild != nil {
		return n.lastChild
	}

	return nil
}

func (n *node) NextSibling() tree.Node {
	if n != nil && n.nextSibling != nil {
		return n.nextSibling
	}

	return nil
}

func (n *node) PrevSibling() tree.Node {
	if n != nil && n.prevSibling != nil {
		return n.prevSibling
	}

	return nil
}

func (n *node) Parent() tree.Node {
	if n != nil && n.parent != nil {
		return n.parent
	}

	return nil
}

func (n *node) InsertBefore(target, ref tree.Node) {
	var (
		newChild   *node = target.(*node)
		oldChild   *node = ref.(*node)
		prev, next *node
	)

	if newChild.parent != nil {
		newChild.parent.RemoveChild(target)
	}

	if oldChild != nil {
		prev, next = oldChild.prevSibling, oldChild
	} else {
		prev = n.lastChild
	}

	if prev != nil {
		prev.nextSibling = newChild
	} else {
		n.firstChild = newChild
	}

	if next != nil {
		next.prevSibling = newChild
	} else {
		n.lastChild = newChild
	}

	newChild.parent = n
	newChild.prevSibling = prev
	newChild.nextSibling = next
}

func (n *node) AppendChild(target tree.Node) {
	var c *node = target.(*node)

	if c.parent != nil {
		c.parent.RemoveChild(target)
	}

	last := n.lastChild
	if last != nil {
		last.nextSibling = c
	} else {
		n.firstChild = c
	}

	n.lastChild = c

	c.parent = n
	c.prevSibling = last
}

func (n *node) RemoveChild(target tree.Node) {
	var c *node = target.(*node)

	if c.parent != n {
		panic(`morphtree: RemoveChild: this Node object is not child node of parent node`)
	}

	if n.firstChild == c {
		n.firstChild = c.nextSibling
	}

	if c.nextSibling != nil {
		c.nextSibling.prevSibling = c.prevSibling
	}

	if n.lastChild == c {
		n.lastChild = c.prevSibling
	}

	if c.prevSibling != nil {
		c.prevSibling.nextSibling = c.nextSibling
	}

	c.parent = nil
	c.prevSibling = nil
	c.nextSibling = nil
}

func (n *node) ReplaceChild(target, ref tree.Node) {
	n.InsertBefore(target, ref)
	n.RemoveChild(ref)
}

func (n *node) TextContent() string {
	if n.kind != tree.TextNode && n.kind != tree.CommentNode {
		panic(`morphtree: TextContent: this node is not Text value node.`)
	}

	return n.data
}

func (n *node) SetTextContent(content string) {
	if n.kind == tree.UnknownNode {
		n.kind = tree.TextNode
	}

	if n.kind != tree.TextNode && n.kind != tree.CommentNode {
		panic(`morphtree: SetTextContent: this node is not Text value node.`)
	}

	n.data = content
}

func (n *node) Attrs() []tree.Attribute {
	if n == nil {
		return make([]tree.Attribute, 0)
	}

	if len(n.attrs) == 0 {
		return make([]tree.Attribute, 0)
	}

	ret := make([]tree.Attribute, len(n.attrs))

	for idx, attr := range n.attrs {
		ret[idx] = tree.Attribute(attr)
	}

	return ret
}

func (n *node) HasAttr(key string) bool {
	if n == nil {
		return false
	}

	if len(n.attrs) == 0 {
		return false
	}

	for _, attr := range n.attrs {
		if attr.key == key {
			return true
		}
	}

	return false
}

func (n *node) GetAttr(key string) tree.Attribute {
	if n == nil {
		return nil
	}

	if len(n.attrs) == 0 {
		return nil
	}

	for _, attr := range n.attrs {
		if attr.key == key {
			return tree.Attribute(attr)
		}
	}

	return nil
}

func (n *node) RemoveAttr(key string) {
	if n.HasAttr(key) {
		newAttr := make([]*attribute, len(n.attrs)-1)
		newIdx := 0

		for _, attr := range n.attrs {
			if attr.key == key {
				continue
			}

			newAttr[newIdx] = attr
			newIdx = newIdx + 1
		}

		n.attrs = newAttr
	}
}

func (n *node) SetAttr(key, val string) {
	if n != nil && len(n.attrs) > 0 {
		for _, attr := range n.attrs {
			if attr.key == key {
				attr.value = val
				return
			}
		}
	}

	n.attrs = append(n.attrs, tree.CreateAttribute(key, val).(*attribute))
}

func (n *node) HasAttrNS(namespace, key string) bool {
	if n == nil {
		return false
	}

	if len(n.attrs) == 0 {
		return false
	}

	idx := strings.Index(key, `:`)
	prefix := ``
	name := ``
	if idx > 0 {
		prefix = key[0:idx]
		name = key[idx+1:]
	} else {
		name = key
	}

	for _, attr := range n.attrs {
		if attr.namespace == namespace && attr.prefix == prefix && attr.name == name {
			return true
		}
	}

	return false
}

func (n *node) GetAttrNS(namespace, key string) tree.Attribute {
	if n == nil {
		return nil
	}

	if len(n.attrs) == 0 {
		return nil
	}

	idx := strings.Index(key, `:`)
	prefix := ``
	name := ``
	if idx > 0 {
		prefix = key[0:idx]
		name = key[idx+1:]
	} else {
		name = key
	}

	for _, attr := range n.attrs {
		if attr.namespace == namespace && attr.prefix == prefix && attr.name == name {
			return attr
		}
	}

	return nil
}

func (n *node) RemoveAttrNS(namespace, key string) {
	if n == nil {
		return
	}

	if len(n.attrs) == 0 {
		return
	}

	idx := strings.Index(key, `:`)
	prefix := ``
	name := ``
	if idx > 0 {
		prefix = key[0:idx]
		name = key[idx+1:]
	} else {
		name = key
	}

	newAttr := make([]*attribute, len(n.attrs)-1)
	newIdx := 0
	for _, attr := range n.attrs {
		if attr.namespace == namespace && attr.prefix == prefix && attr.name == name {
			continue
		}

		newAttr[newIdx] = attr
		newIdx = newIdx + 1
	}

	n.attrs = newAttr
}

func (n *node) SetAttrNS(namespace, key, val string) {
	idx := strings.Index(key, `:`)
	prefix := ``
	name := ``
	if idx > 0 {
		prefix = key[0:idx]
		name = key[idx+1:]
	} else {
		name = key
	}

	if n != nil && len(n.attrs) > 0 {
		for idx, attr := range n.attrs {
			if attr.namespace == namespace && attr.prefix == prefix && attr.name == name {
				n.attrs[idx] = tree.CreateAttributeNS(namespace, key, val).(*attribute)
				return
			}
		}
	}

	n.attrs = append(n.attrs, tree.CreateAttributeNS(namespace, key, val).(*attribute))
}

func (n *node) IsSameNode(target tree.Node) bool {
	t := target.(*node)

	if n == nil && t == nil {
		return true
	}

	if n == nil && t != nil {
		return false
	}

	if n != nil && t == nil {
		return false
	}

	if n.kind != t.kind {
		return false
	}

	if n.kind == tree.UnknownNode {
		return false
	}

	if n.kind == tree.TextNode || n.kind == tree.CommentNode {
		return n.data == t.data
	}

	if n.parent != nil && t.parent == nil {
		return false
	}

	if n.parent == nil && t.parent != nil {
		return false
	}

	if n.parent != t.parent {
		return false
	}

	for nc, tc := n.firstChild, t.firstChild; nc != nil || tc != nil; nc, tc = nc.nextSibling, tc.nextSibling {
		if nc == nil && tc != nil {
			return false
		}

		if nc != nil && tc == nil {
			return false
		}

		if nc == nil && tc == nil {
			break
		}

		if nc.kind != tc.kind {
			return false
		}

		if nc.kind == tree.UnknownNode {
			return false
		}

		if nc.kind == tree.TextNode && nc.data != tc.data {
			return false
		}

		if !nc.IsSameNode(tree.Node(tc)) {
			return false
		}
	}

	return true
}

func (node *node) On(ev string, fn func(target tree.Node) tree.Node) {
	if node != nil {
		node.events[ev] = fn
	}
}

func (node *node) Off(ev string) {
	if node != nil {
		if _, exists := node.events[ev]; exists {
			delete(node.events, ev)
		}
	}
}

func (node *node) Trigger(ev string) {
	if node != nil {
		if fn, exists := node.events[ev]; exists {
			morph.New().Morph(node, fn(node), false)
		}
	}
}
