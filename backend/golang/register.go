package golang

import (
	"strings"

	"github.com/nyarla/morphtree/tree"
)

func init() {
	tree.RegisterBackend(new(backend))
}

type backend struct{}

func (b *backend) CreateTextNode(content string) tree.Node {
	return &node{
		kind: tree.TextNode,
		data: content,
	}
}

func (b *backend) CreateComment(content string) tree.Node {
	return &node{
		kind: tree.CommentNode,
		data: content,
	}
}

func (b *backend) CreateElement(tag string) tree.Node {
	return &node{
		kind:      tree.ElementNode,
		namespace: ``,
		prefix:    ``,
		data:      tag,
		events:    make(map[string]func(tree.Node) tree.Node),
	}
}

func (b *backend) CreateElementNS(nsURI, tag string) tree.Node {
	el := new(node)
	el.kind = tree.ElementNode
	el.namespace = nsURI
	el.events = make(map[string]func(tree.Node) tree.Node)

	idx := strings.Index(tag, `:`)
	if idx > 0 {
		el.prefix = tag[0:idx]
		el.data = tag[idx+1:]
	} else {
		el.data = tag
	}

	return el
}

func (b *backend) CreateAttribute(key, val string) tree.Attribute {
	return &attribute{
		key:       key,
		name:      key,
		prefix:    ``,
		namespace: ``,
		value:     val,
	}
}

func (b *backend) CreateAttributeNS(nsURI, key, val string) tree.Attribute {
	attr := new(attribute)
	attr.key = key
	attr.value = val
	attr.namespace = nsURI

	idx := strings.Index(key, `:`)
	if idx > 0 {
		attr.prefix = key[0:idx]
		attr.name = key[idx+1:]
	} else {
		attr.name = key
	}

	return attr
}
