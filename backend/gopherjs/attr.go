// +build js

package gopherjs

import (
	"github.com/gopherjs/gopherjs/js"
)

type attribute struct {
	js *js.Object
}

func (a *attribute) Namespace() string {
	if a != nil && a.js != nil {
		if v := a.js.Get(`namespaceURI`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (a *attribute) Prefix() string {
	if a != nil && a.js != nil {
		if v := a.js.Get(`prefix`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (a *attribute) Name() string {
	if a != nil && a.js != nil {
		if v := a.js.Get(`localName`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (a *attribute) Key() string {
	if a != nil && a.js != nil {
		if v := a.js.Get(`name`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (a *attribute) Value() string {
	if a != nil && a.js != nil {
		if v := a.js.Get(`nodeValue`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}
