// +build js

package gopherjs

import (
	"testing"

	"github.com/gopherjs/gopherjs/js"

	"github.com/nyarla/morphtree/tree"
)

func TestAttrJS(t *testing.T) {
	jsdom := js.Global.Call(`require`, `jsdom`)
	window := jsdom.Get(`JSDOM`).New(`<!DOCTYPE html><html><head></head><body></body></html>`).Get(`window`)
	document = window.Get(`document`)

	attr := tree.CreateAttribute(`foo`, `bar`)

	if attr.Namespace() != `` {
		t.Fatal(`this value should be empty string (attr.Namespace)`)
	}

	if attr.Prefix() != `` {
		t.Fatal(`this value should be empty string (attr.Prefix)`)
	}

	if attr.Name() != `foo` {
		t.Fatal(`this value should be string of 'foo' (attr.Name)`)
	}

	if attr.Key() != `foo` {
		t.Fatal(`this value should be string of 'foo' (attr.Key)`)
	}

	if attr.Value() != `bar` {
		t.Fatal(`thsivalue should be string of 'bar' (attr.Value)`)
	}

	attrNS := tree.CreateAttributeNS(`https://example.com/`, `foo:bar`, `value`)

	if attrNS.Namespace() != `https://example.com/` {
		t.Fatal(`this value should be string of 'http://example.com' (attrNS.Namespace)`)
	}

	if attrNS.Prefix() != `foo` {
		t.Fatal(`this value should be string of 'foo' (attrNS.Prefix)`)
	}

	if attrNS.Name() != `bar` {
		t.Fatal(`this value should be string of 'bar' (attrNS.Name)`)
	}

	if attrNS.Key() != `foo:bar` {
		t.Fatal(`this value should be string of 'foo:bar' (attrNS.Key)`)
	}

	if attrNS.Value() != `value` {
		t.Fatal(`this value should be string of 'value' (attrNS.Value)`)
	}

}
