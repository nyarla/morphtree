// +build js

package gopherjs

import (
	"github.com/gopherjs/gopherjs/js"

	"github.com/nyarla/morphtree/morph"
	"github.com/nyarla/morphtree/tree"
)

func syncBooleanAttrProp(from, to tree.Node, prop string) {
	fAttr := from.GetAttr(prop)
	tAttr := from.GetAttr(prop)

	if tAttr != nil {
		if fAttr == nil || (fAttr != nil && fAttr.Value() != tAttr.Value()) {
			from.SetAttr(prop, tAttr.Value())
		}
	} else {
		from.RemoveAttr(prop)
	}
}

func getProp(target tree.Node, key string) string {
	if c := target.(*node); c != nil && c.js != nil {
		if v := c.js.Get(key); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func setProp(target tree.Node, key string, val interface{}) {
	if c := target.(*node); c != nil && c.js != nil {
		c.js.Set(key, val)
	}
}

func init() {
	morph.DefaultIgnoreNodes = []string{`textarea`}
	morph.DefaultSpecialHandlers = map[string]morph.HandlerFunc{
		`option`: morph.HandlerFunc(func(from, to tree.Node) {
			syncBooleanAttrProp(from, to, `selected`)
		}),
		`input`: morph.HandlerFunc(func(from, to tree.Node) {
			syncBooleanAttrProp(from, to, `checked`)
			syncBooleanAttrProp(from, to, `disabled`)

			fv, tv := getProp(from, `value`), getProp(from, `value`)

			if tv != `` {
				if fv == `` || (fv != `` && fv != tv) {
					setProp(from, `value`, tv)
				}
			} else {
				setProp(from, `value`, ``)
			}
		}),
		`textarea`: morph.HandlerFunc(func(from, to tree.Node) {
			fv, tv := getProp(from, `value`), getProp(from, `value`)

			if tv != `` {
				if fv == `` || (fv != `` && fv != tv) {
					setProp(from, `value`, tv)

					if firstChild := from.FirstChild(); firstChild != nil {
						val := getProp(firstChild, `nodeValue`)
						if val == tv || (tv != `` && getProp(from, `placeholder`) != ``) {
							return
						}

						setProp(firstChild, `nodeValue`, tv)
					}
				}
			} else {
				setProp(from, `value`, ``)
			}
		}),
		`select`: morph.HandlerFunc(func(from, to tree.Node) {
			if to.HasAttr(`multiple`) {
				selectedIdx := -1
				idx := 1

				curChild := to.FirstChild()
				for curChild != nil {
					name := curChild.Name()
					if name != `` && name == `options` {
						if curChild.HasAttr(`selected`) {
							selectedIdx = idx
							break
						}
						idx = idx + 1
					}

					curChild = curChild.NextSibling()
				}

				setProp(from, `selectedIndex`, selectedIdx)
			}
		}),
	}
}
