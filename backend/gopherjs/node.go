// +build js

package gopherjs

import (
	"github.com/gopherjs/gopherjs/js"

	"github.com/nyarla/morphtree/morph"
	"github.com/nyarla/morphtree/tree"
)

type node struct {
	js     *js.Object
	events map[string]*js.Object
}

func (n *node) Namespace() string {
	if n != nil && n.js != nil {
		if v := n.js.Get(`namespaceURI`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (n *node) Type() tree.NodeType {
	if n != nil && n.js != nil {
		if v := n.js.Get(`nodeType`); v != nil && v != js.Undefined {
			switch v.Int() {
			case 1:
				return tree.ElementNode
			case 3:
				return tree.TextNode
			case 8:
				return tree.CommentNode
			default:
				return tree.UnsupportedNode
			}
		}
	}

	return tree.UnknownNode
}

func (n *node) Prefix() string {
	if n != nil && n.js != nil {
		if v := n.js.Get(`prefix`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (n *node) Name() string {
	if n != nil && n.js != nil {
		if v := n.js.Get(`nodeName`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (n *node) Key() string {
	if n != nil && n.js != nil {
		if v := n.js.Get(`id`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (n *node) FirstChild() tree.Node {
	if n != nil && n.js != nil {
		if v := n.js.Get(`firstChild`); v != nil && v != js.Undefined {
			return &node{v, make(map[string]*js.Object)}
		}
	}

	return nil
}

func (n *node) LastChild() tree.Node {
	if n != nil && n.js != nil {
		if v := n.js.Get(`lastChild`); v != nil && v != js.Undefined {
			return &node{v, make(map[string]*js.Object)}
		}
	}

	return nil
}

func (n *node) NextSibling() tree.Node {
	if n != nil && n.js != nil {
		if v := n.js.Get(`nextSibling`); v != nil && v != js.Undefined {
			return &node{v, make(map[string]*js.Object)}
		}
	}

	return nil
}

func (n *node) PrevSibling() tree.Node {
	if n != nil && n.js != nil {
		if v := n.js.Get(`previousSibling`); v != nil && v != js.Undefined {
			return &node{v, make(map[string]*js.Object)}
		}
	}

	return nil
}

func (n *node) Parent() tree.Node {
	if n != nil && n.js != nil {
		if v := n.js.Get(`parentNode`); v != nil && v != js.Undefined {
			return &node{v, make(map[string]*js.Object)}
		}
	}

	return nil
}

func (n *node) InsertBefore(target, ref tree.Node) {
	if n != nil && n.js != nil {
		var (
			newChild *node = target.(*node)
			oldChild *node = ref.(*node)
		)

		n.js.Call(`insertBefore`, newChild.js, oldChild.js)
	}
}

func (n *node) AppendChild(target tree.Node) {
	if n != nil && n.js != nil {
		var c *node = target.(*node)

		n.js.Call(`appendChild`, c.js)
	}
}

func (n *node) RemoveChild(target tree.Node) {
	if n != nil && n.js != nil {
		var c *node = target.(*node)

		n.js.Call(`removeChild`, c.js)

	}
}

func (n *node) ReplaceChild(target, ref tree.Node) {
	if n != nil && n.js != nil {
		var (
			newChild *node = target.(*node)
			oldChild *node = ref.(*node)
		)

		n.js.Call(`replaceChild`, newChild.js, oldChild.js)
	}
}

func (n *node) TextContent() string {
	if n != nil && n.js != nil {
		if v := n.js.Get(`textContent`); v != nil && v != js.Undefined {
			return v.String()
		}
	}

	return ``
}

func (n *node) SetTextContent(content string) {
	if n != nil && n.js != nil {
		n.js.Set(`textContent`, content)
	}
}

func (n *node) Attrs() []tree.Attribute {
	if n != nil && n.js != nil {
		if v := n.js.Get(`attributes`); v != nil && v != js.Undefined {
			if l := v.Length(); l > 0 {
				result := make([]tree.Attribute, l)

				for idx := 0; idx < l; idx++ {
					result[idx] = &attribute{v.Index(idx)}
				}

				return result
			}
		}
	}

	return make([]tree.Attribute, 0)
}

func (n *node) HasAttr(key string) bool {
	if n != nil && n.js != nil {
		return n.js.Call(`hasAttribute`, key).Bool()
	}

	return false
}

func (n *node) GetAttr(key string) tree.Attribute {
	if n != nil && n.js != nil {
		if v := n.js.Call(`getAttributeNode`, key); v != nil && v != js.Undefined {
			return &attribute{v}
		}
	}

	return nil
}

func (n *node) RemoveAttr(key string) {
	if n != nil && n.js != nil {
		n.js.Call(`removeAttribute`, key)
	}
}

func (n *node) SetAttr(key, val string) {
	if n != nil && n.js != nil {
		n.js.Call(`setAttribute`, key, val)
	}
}

func (n *node) HasAttrNS(nsURI, key string) bool {
	if n != nil && n.js != nil {
		return n.js.Call(`hasAttributeNS`, nsURI, key).Bool()
	}

	return false
}

func (n *node) GetAttrNS(nsURI, key string) tree.Attribute {
	if n != nil && n.js != nil {
		if v := n.js.Call(`getAttributeNodeNS`, nsURI, key); v != nil && v != js.Undefined {
			return &attribute{v}
		}
	}

	return nil
}

func (n *node) RemoveAttrNS(nsURI, key string) {
	if n != nil && n.js != nil {
		n.js.Call(`removeAttributeNS`, nsURI, key)
	}
}

func (n *node) SetAttrNS(nsURI, key, val string) {
	if n != nil && n.js != nil {
		n.js.Call(`setAttributeNS`, nsURI, key, val)
	}
}

func (n *node) IsSameNode(target tree.Node) bool {
	if n != nil && n.js != nil {
		var c *node = target.(*node)

		if b := n.js.Call(`isEqualNode`, c.js); b != nil && b != js.Undefined {
			return b.Bool()
		}
	}

	return false
}

func (n *node) On(ev string, fn func(target tree.Node) tree.Node) {
	if n != nil && n.js != nil {
		n.events[ev] = js.MakeFunc(func(_ *js.Object, _ []*js.Object) interface{} {
			go morph.New().Morph(n, fn(n), false)
			return nil
		})

		n.js.Call(`addEventListener`, ev, n.events[ev])
	}
}

func (n *node) Off(ev string) {
	if n != nil && n.js != nil {
		if fn, exists := n.events[ev]; exists {
			n.js.Call(`removeEventListener`, ev, fn)
			delete(n.events, ev)
		}
	}
}

func (n *node) Trigger(ev string) {
	if n != nil && n.js != nil {
		if fn, exists := n.events[ev]; exists {
			go fn.Invoke(n.js, []*js.Object{})
		}
	}
}
