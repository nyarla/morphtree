// +build js

package gopherjs

import (
	"testing"

	"github.com/gopherjs/gopherjs/js"

	"github.com/nyarla/morphtree/tree"
)

func TestNodeManipulation(t *testing.T) {
	jsdom := js.Global.Call(`require`, `jsdom`)
	window := jsdom.Get(`JSDOM`).New(`<!DOCTYPE html><html><head></head><body></body></html>`).Get(`window`)
	document = window.Get(`document`)

	nodeA := tree.CreateElementNS(`http://example.com/namespace/1.0`, `test:tag`)
	nodeB := tree.CreateElement(`icon`)
	nodeC := tree.CreateTextNode(`message`)
	nodeD := tree.CreateComment(`this is a comment.`)

	nodeA.AppendChild(nodeB)
	nodeA.AppendChild(nodeC)
	nodeA.AppendChild(nodeD)

	if nodeA.Namespace() != `http://example.com/namespace/1.0` {
		t.Fatal(`failed to set namespace URI on nodeA.`)
	}

	if nodeA.Prefix() != `test` {
		t.Fatal(`failed to set tag prefix on nodeA.`)
	}

	if nodeA.Name() != `test:tag` {
		t.Fatal(`failed to set tag name on nodeA.`)
	}

	if !nodeA.FirstChild().IsSameNode(nodeB) {
		t.Fatal(`failed to set FirstChild node on nodeA.`)
	}

	if !nodeA.LastChild().IsSameNode(nodeD) {
		t.Fatal(`failed to set LastChild node on nodeA.`)
	}

	if !nodeA.FirstChild().NextSibling().IsSameNode(nodeC) {
		t.Fatal(`failed to set NextSibling node.`)
	}

	if !nodeA.LastChild().PrevSibling().IsSameNode(nodeC) {
		t.Fatal(`failed to set PrevSibling node.`)
	}

	if !nodeA.FirstChild().Parent().IsSameNode(nodeA) {
		t.Fatal(`failed to set Parent node`)
	}

	if !nodeA.FirstChild().NextSibling().Parent().IsSameNode(nodeA) {
		t.Fatal(`failed to set Parent node`)
	}

	if !nodeA.LastChild().Parent().IsSameNode(nodeA) {
		t.Fatal(`failed to set Parent node`)
	}

	if nodeC.TextContent() != `message` {
		t.Fatal(`failed to set TextContent`)
	}

	if nodeD.TextContent() != `this is a comment.` {
		t.Fatal(`failed to set TextContent`)
	}

	nodeA.RemoveChild(nodeB)
	nodeA.AppendChild(nodeB)

	if !nodeA.FirstChild().IsSameNode(nodeC) {
		t.Fatal(`failed to call RemoveChild -> AppendChild.`)
	}

	if !nodeA.LastChild().IsSameNode(nodeB) {
		t.Fatal(`failedto call RemoveChild -> AppendChild.`)
	}

	nodeA.RemoveChild(nodeD)
	nodeA.InsertBefore(nodeD, nodeC)

	if !nodeA.FirstChild().IsSameNode(nodeD) {
		t.Fatal(`failed to call RemoveChild -> InsertBefore.`)
	}

	if !nodeA.LastChild().PrevSibling().IsSameNode(nodeC) {
		t.Fatal(`failed to call RemoveChild -> InsertBefore.`)
	}

	nodeA.RemoveChild(nodeC)
	nodeA.ReplaceChild(nodeC, nodeB)

	if !nodeA.LastChild().IsSameNode(nodeC) {
		t.Fatal(`failed to call RemoveChild -> ReplaceChild.`)
	}
}

func TestNodeAttributes(t *testing.T) {
	jsdom := js.Global.Call(`require`, `jsdom`)
	window := jsdom.Get(`JSDOM`).New(`<!DOCTYPE html><html><head></head><body></body></html>`).Get(`window`)
	document = window.Get(`document`)

	nodeA := tree.CreateElement(`test`)
	nodeA.SetAttr(`foo`, `bar`)

	if !nodeA.HasAttr(`foo`) {
		t.Fatal(`failed to set attribute`)
	}

	if attr := nodeA.GetAttr(`foo`); attr == nil || attr.Value() != `bar` {
		t.Fatal(`failed to get attribute`)
	}

	nodeA.RemoveAttr(`foo`)

	if len(nodeA.Attrs()) != 0 {
		t.Fatal(`failed to remove attribute`)
	}

	nsURI := `http://example.com/namespace/1.0`

	nodeB := tree.CreateElementNS(nsURI, `test:tag`)
	nodeB.SetAttrNS(nsURI, `test:attr`, `value`)

	if !nodeB.HasAttrNS(nsURI, `attr`) {
		t.Fatal(`failed to set attribute with namespace`)
	}

	if nodeB.HasAttrNS(``, `attr`) {
		t.Fatal(`failed to filter attribute in namespace`)
	}
}
