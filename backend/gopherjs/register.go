// +build js

package gopherjs

import (
	"github.com/gopherjs/gopherjs/js"

	"github.com/nyarla/morphtree/tree"
)

var document *js.Object

func init() {
	if doc := js.Global.Get(`document`); doc != nil && doc != js.Undefined {
		document = doc
	}

	tree.RegisterBackend(new(backend))
}

type backend struct{}

func (b *backend) CreateTextNode(content string) tree.Node {
	return &node{document.Call(`createTextNode`, content), make(map[string]*js.Object)}
}

func (b *backend) CreateComment(content string) tree.Node {
	return &node{document.Call(`createComment`, content), make(map[string]*js.Object)}
}

func (b *backend) CreateElement(tag string) tree.Node {
	return &node{document.Call(`createElement`, tag), make(map[string]*js.Object)}
}

func (b *backend) CreateElementNS(nsURI, tag string) tree.Node {
	return &node{document.Call(`createElementNS`, nsURI, tag), make(map[string]*js.Object)}
}

func (b *backend) CreateAttribute(key, val string) tree.Attribute {
	attr := document.Call(`createAttribute`, key)
	attr.Set(`value`, val)

	return &attribute{attr}
}

func (b *backend) CreateAttributeNS(nsURI, key, val string) tree.Attribute {
	attr := document.Call(`createAttributeNS`, nsURI, key)
	attr.Set(`value`, val)

	return &attribute{attr}
}
