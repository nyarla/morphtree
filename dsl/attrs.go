package dsl

import (
	"github.com/nyarla/morphtree/tree"
)

type AttributesDescriber map[string]string

func (a AttributesDescriber) Describe(target tree.Node) tree.Node {
	for key, val := range a {
		target.SetAttr(key, val)
	}

	return target
}

type A map[string]string

func (a A) Describe(target tree.Node) tree.Node {
	return AttributesDescriber(a).Describe(target)
}
