package dsl

import (
	"testing"

	_ "github.com/nyarla/morphtree/backend/golang"
	"github.com/nyarla/morphtree/tree"
)

func TestAttributesDescriber(t *testing.T) {
	desc := A{`foo`: `bar`}
	node := tree.CreateElement(`tag`)

	if node = desc.Describe(node); node.GetAttr(`foo`).Value() != `bar` {
		t.Fatal(`failed to describe attributes.`)
	}

}
