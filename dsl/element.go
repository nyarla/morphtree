package dsl

import (
	"github.com/nyarla/morphtree/tree"
)

type ElementDescriber struct {
	Name     string
	Contents []Describer
}

func (e *ElementDescriber) Describe(_ tree.Node) tree.Node {
	node := tree.CreateElement(e.Name)
	for _, desc := range e.Contents {
		switch desc.(type) {
		case AttributesDescriber, A:
			desc.Describe(node)
		default:
			node.AppendChild(desc.Describe(tree.CreateTextNode(``)))
		}
	}

	return node
}

func H(src string, contents ...Describer) Describer {
	tag, attr := ParseTag(src)

	if len(attr) > 0 {
		contents = append(contents, A(attr))
	}

	return &ElementDescriber{Name: tag, Contents: contents}
}
