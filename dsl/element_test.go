package dsl

import (
	"testing"

	_ "github.com/nyarla/morphtree/backend/golang"
	"github.com/nyarla/morphtree/tree"
)

func TestElementDescriber(t *testing.T) {
	node := H(`p#msg`, T(`hello, world!`)).Describe(tree.CreateElement(`p`))

	if node.Name() != `p` {
		t.Fatalf(`failed to describe element: %+v`, node)
	}

	if node.GetAttr(`id`).Value() != `msg` {
		t.Fatalf(`failed to describe element: %+v`, node)
	}

	if node.FirstChild().TextContent() != `hello, world!` {
		t.Fatalf(`failed to describe element: %+v`, node)
	}
}
