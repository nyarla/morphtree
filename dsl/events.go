package dsl

import (
	"github.com/nyarla/morphtree/tree"
)

type EventsDescriber map[string]func(target tree.Node) tree.Node

func (e EventsDescriber) Describe(target tree.Node) tree.Node {
	if target.Type() != tree.ElementNode {
		return target
	}

	for ev, fn := range e {
		target.On(ev, fn)
	}

	return target
}

type E map[string]func(target tree.Node) tree.Node

func (e E) Describe(target tree.Node) tree.Node {
	return EventsDescriber(e).Describe(target)
}
