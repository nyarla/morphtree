package dsl

import (
	"github.com/nyarla/morphtree/tree"
)

type Describer interface {
	Describe(target tree.Node) tree.Node
}
