package dsl

import (
	"bytes"
	"sort"
	"strings"
)

func ParseTag(src string) (string, map[string]string) {
	r := strings.NewReader(src)
	b := new(bytes.Buffer)
	k := `tag`
	id := ``
	tag := ``
	classes := make(map[string]bool)

read:
	for {
		char, _, err := r.ReadRune()
		if char == '#' || char == '.' || err != nil {
			val := b.String()

			switch {
			case k == `id`:
				id = val
			case k == `class`:
				classes[val] = true
			default:
				if val != `` {
					tag = val
				}
			}

			b = new(bytes.Buffer)

			switch {
			case char == '#':
				k = `id`
				continue
			case char == '.':
				k = `class`
				continue
			case err != nil:
				break read
			}
		}

		b.WriteRune(char)
	}

	attr := make(map[string]string)

	if id != `` {
		attr[`id`] = id
	}

	if l := len(classes); l > 0 {
		cls := make([]string, l)
		idx := 0

		for class, val := range classes {
			if val {
				cls[idx] = class
				idx = idx + 1
			}
		}

		sort.Strings(cls)
		attr[`class`] = strings.Join(cls, ` `)
	}

	return tag, attr
}
