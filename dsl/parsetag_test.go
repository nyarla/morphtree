package dsl

import (
	"testing"

	_ "github.com/nyarla/morphtree/backend/golang"
	"github.com/nyarla/morphtree/tree"
)

func TestParseTag(t *testing.T) {
	if tag, _ := ParseTag(`p`); tag != `p` {
		t.Fatal(`failed to parse tag name.`)
	}

	if tag, attr := ParseTag(`p#id`); tag != `p` {
		t.Fatal(`failed to parse tag name with id`)
	} else {
		node := A(attr).Describe(tree.CreateElement(tag))

		if len(node.Attrs()) != 1 || node.GetAttr(`id`).Value() != `id` {
			t.Fatalf(`failed to parse tag name with id: %+v`, node)
		}
	}

	if tag, attr := ParseTag(`p.foo.bar`); tag != `p` {
		t.Fatal(`failed to parse tag name with classes`)
	} else {
		node := A(attr).Describe(tree.CreateElement(tag))

		if len(node.Attrs()) != 1 || node.GetAttr(`class`).Value() != `bar foo` {
			t.Fatalf(`failed to parse tag name with classes: %+v`, node)
		}
	}

	if tag, attr := ParseTag(`div#id.foo.bar`); tag != `div` {
		t.Fatalf(`failed to parse id and classes without tag name: %+v`, tag)
	} else {
		node := A(attr).Describe(tree.CreateElement(tag))

		if len(node.Attrs()) != 2 || node.GetAttr(`id`).Value() != `id` || node.GetAttr(`class`).Value() != `bar foo` {
			t.Fatalf(`failed to parse id and classes without tag name: %+v`, node)
		}
	}
}
