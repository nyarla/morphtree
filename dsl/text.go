package dsl

import (
	"github.com/nyarla/morphtree/tree"
)

type TextDescriber string

func (t TextDescriber) Describe(target tree.Node) tree.Node {
	if target.Type() != tree.TextNode && target.Type() != tree.CommentNode {
		return tree.CreateTextNode(string(t))
	}

	target.SetTextContent(string(t))
	return target
}

type T string

func (t T) Describe(target tree.Node) tree.Node {
	return TextDescriber(t).Describe(target)
}
