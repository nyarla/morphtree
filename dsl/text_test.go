package dsl

import (
	"testing"

	_ "github.com/nyarla/morphtree/backend/golang"
	"github.com/nyarla/morphtree/tree"
)

func TestTextDescriber(t *testing.T) {
	desc := T(`hello, world!`)

	if result := desc.Describe(tree.CreateElement(`tag`)); result.Type() != tree.TextNode && result.TextContent() != `hello, world!` {
		t.Fatal(`failed to describe text node.`)
	}

	if result := desc.Describe(tree.CreateComment(`hi`)); result.Type() != tree.CommentNode && result.TextContent() != `hello, world!` {
		t.Fatal(`failed to describe text node.`)
	}
}
