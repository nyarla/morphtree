package morph

import (
	"github.com/nyarla/morphtree/tree"
)

type HandlerFunc func(from, to tree.Node)

type Events struct {
	OnBeforeNodeAdded              func(node tree.Node) tree.Node
	OnNodeAdded                    func(node tree.Node)
	OnBeforeElementUpdated         func(from, to tree.Node) bool
	OnBeforeElementChildrenUpdated func(from, to tree.Node) bool
	OnElementUpdated               func(node tree.Node)
	OnBeforeNodeDiscarded          func(node tree.Node) bool
	OnNodeDiscarded                func(node tree.Node)
}

var (
	DefaultEvents = &Events{
		OnBeforeNodeAdded:              func(node tree.Node) tree.Node { return node },
		OnNodeAdded:                    func(_ tree.Node) {},
		OnBeforeElementUpdated:         func(_, _ tree.Node) bool { return true },
		OnBeforeElementChildrenUpdated: func(_, _ tree.Node) bool { return true },
		OnElementUpdated:               func(_ tree.Node) {},
		OnBeforeNodeDiscarded:          func(_ tree.Node) bool { return true },
		OnNodeDiscarded:                func(_ tree.Node) {},
	}
	DefaultIgnoreNodes     = []string{}
	DefaultSpecialHandlers = make(map[string]HandlerFunc)
)
