package morph

import (
	"container/list"

	"github.com/nyarla/morphtree/tree"
)

type Morph struct {
	skip             []string
	events           *Events
	handlers         map[string]HandlerFunc
	fromNodeLookup   map[string]tree.Node
	keyedRemovalList *list.List
}

func New() *Morph {
	return &Morph{
		skip:             DefaultIgnoreNodes,
		events:           DefaultEvents,
		handlers:         DefaultSpecialHandlers,
		fromNodeLookup:   make(map[string]tree.Node),
		keyedRemovalList: list.New(),
	}
}

func (this *Morph) SetEventHandlers(ev *Events) *Morph {
	this.events = ev
	return this
}

func (this *Morph) SetIgnoreNodes(ignore []string) *Morph {
	this.skip = ignore
	return this
}

func (this *Morph) SetSpecialHandlers(fn map[string]HandlerFunc) *Morph {
	this.handlers = fn
	return this
}

func (this *Morph) indexing(target tree.Node) {
	if target.Type() == tree.ElementNode {
		for current := target.FirstChild(); current != nil; current = current.NextSibling() {
			if key := current.Key(); key != `` {
				this.fromNodeLookup[key] = current
			}

			this.indexing(current)
		}
	}
}

func (this *Morph) moveChildren(from, to tree.Node) tree.Node {
	current := from.FirstChild()

	for current != nil {
		next := current.NextSibling()
		to.AppendChild(current)
		current = next
	}

	return to
}

func (this *Morph) walkDiscardedChildNodes(target tree.Node, skipKeyedNodes bool) {
	if target.Type() == tree.ElementNode {
		for current := target.FirstChild(); current != nil; current = current.NextSibling() {
			if key := current.Key(); key != `` {
				this.keyedRemovalList.PushBack(key)
			} else {
				this.events.OnNodeDiscarded(current)

				if current.FirstChild() != nil {
					this.walkDiscardedChildNodes(current, skipKeyedNodes)
				}
			}
		}
	}
}

func (this *Morph) removeNode(target, parent tree.Node, skipKeyedNodes bool) {
	if !this.events.OnBeforeNodeDiscarded(target) {
		return
	}

	if parent != nil {
		parent.RemoveChild(target)
	}

	this.events.OnNodeDiscarded(target)
	this.walkDiscardedChildNodes(target, skipKeyedNodes)
}

func (this *Morph) handleNodeAdded(target tree.Node) {
	this.events.OnNodeAdded(target)

	for current := target.FirstChild(); current != nil; current = current.NextSibling() {
		if key := current.Key(); key != `` {
			if unmatched, found := this.fromNodeLookup[key]; found && current.Name() == unmatched.Name() {
				if parent := current.Parent(); parent != nil {
					parent.ReplaceChild(unmatched, current)
					this.modify(unmatched, current, false)
				}
			}
		}

		this.events.OnNodeAdded(current)
	}
}

func (this *Morph) attrs(attrs []tree.Attribute, target tree.Node) {
	if target.Type() != tree.ElementNode {
		return
	}

	for _, attr := range attrs {
		if nsURI := attr.Namespace(); nsURI != `` {
			target.SetAttrNS(nsURI, attr.Key(), attr.Value())
		} else {
			target.SetAttr(attr.Key(), attr.Value())
		}
	}

filter:
	for _, tAttr := range target.Attrs() {
		nsURI := tAttr.Namespace()
		key := tAttr.Key()

		for _, attr := range attrs {

			if nsURI == `` && attr.Key() == key {
				continue filter
			}

			if nsURI != `` && attr.Namespace() == nsURI && attr.Key() == key {
				continue filter
			}
		}

		if nsURI != `` {
			target.RemoveAttrNS(nsURI, key)
		} else {
			target.RemoveAttr(key)
		}
	}
}

func (this *Morph) modify(from, to tree.Node, childrenOnly bool) {
	if key := to.Key(); key != `` {
		delete(this.fromNodeLookup, key)
	}

	if to.IsSameNode(from) {
		return
	}

	if !childrenOnly {
		if !this.events.OnBeforeElementUpdated(from, to) {
			return
		}

		this.attrs(to.Attrs(), from)
		this.events.OnElementUpdated(from)

		if !this.events.OnBeforeElementChildrenUpdated(from, to) {
			return
		}
	}

	skip := false

	for _, name := range this.skip {
		if name == from.Name() {
			skip = true
			break
		}
	}

	if !skip {

		var (
			curFromChild = from.FirstChild()
			curToChild   = to.FirstChild()

			fromNextSibling tree.Node
			toNextSibling   tree.Node
		)

	outer:
		for curToChild != nil {
			toNextSibling = curToChild.NextSibling()
			curToKey := curToChild.Key()

			for curFromChild != nil {
				if curToChild.IsSameNode(curFromChild) {
					curToChild = toNextSibling
					curFromChild = fromNextSibling
					continue outer
				}

				curFromKey := from.Key()
				curFromType := curFromChild.Type()
				isCompatible := false

				if curFromType == curToChild.Type() {
					switch curFromType {
					case tree.ElementNode:
						if curToKey != `` {
							if curToKey != curFromKey {
								if matched, found := this.fromNodeLookup[curToKey]; found {
									if curFromChild.IsSameNode(matched) {
										isCompatible = false
									} else {
										from.InsertBefore(matched, curFromChild)
										fromNextSibling = curFromChild.NextSibling()

										if curToKey != `` {
											this.keyedRemovalList.PushBack(curToKey)
										} else {
											this.removeNode(curFromChild, from, true)
										}

										curFromChild = matched
									}
								} else {
									isCompatible = false
								}
							}
						} else {
							if curFromKey != `` {
								isCompatible = false
							}

							isCompatible = isCompatible == true && curFromChild.Name() == curToChild.Name()
							if isCompatible {
								this.modify(curFromChild, curToChild, false)
							}
						}
					case tree.TextNode, tree.CommentNode:
						isCompatible = true

						if text := curFromChild.TextContent(); text != curToChild.TextContent() {
							curFromChild.SetTextContent(text)
						}
					}
				}

				if isCompatible {
					curToChild = toNextSibling
					curFromChild = fromNextSibling
					continue outer
				}

				if curFromKey != `` {
					this.keyedRemovalList.PushBack(curFromKey)
				} else {
					this.removeNode(curFromChild, from, true)
				}

				curFromChild = fromNextSibling
			}

			var (
				matched tree.Node
				found   bool
			)

			if curToKey != `` {
				if matched, found = this.fromNodeLookup[curToKey]; found && matched.Name() == curToChild.Name() {
					from.AppendChild(matched)
					this.modify(matched, curToChild, false)
				}
			}

			if curToKey == `` || !found {
				if result := this.events.OnBeforeNodeAdded(curToChild); result != nil {
					curToChild = result
				}

				from.AppendChild(curToChild)
				this.handleNodeAdded(curToChild)
			}

			curToChild = toNextSibling
			curFromChild = fromNextSibling
		}

		for curFromChild != nil {
			fromNextSibling = curFromChild.NextSibling()
			if key := curFromChild.Key(); key != `` {
				this.keyedRemovalList.PushBack(key)
			} else {
				this.removeNode(curFromChild, from, true)
			}

			curFromChild = fromNextSibling
		}

	}

	if fn, ok := this.handlers[from.Name()]; ok {
		fn(from, to)
	}
}

func (this *Morph) Morph(from, to tree.Node, childrenOnly bool) tree.Node {
	this.indexing(from)

	node := from
	nodeType := node.Type()
	toNodeType := to.Type()

	if !childrenOnly {
		switch nodeType {
		case tree.ElementNode:
			if toNodeType == tree.ElementNode {
				if from.Name() != to.Name() {
					this.events.OnNodeDiscarded(from)
					node = this.moveChildren(from, tree.CreateElementNS(to.Namespace(), to.Name()))
				}
			} else {
				node = to
			}
		case tree.TextNode, tree.CommentNode:
			if toNodeType == nodeType {
				if text := to.TextContent(); from.TextContent() != text {
					from.SetTextContent(text)
				}

				return from
			} else {
				node = to
			}
		}
	}

	if node.IsSameNode(to) {
		this.events.OnNodeDiscarded(from)
	} else {
		this.modify(node, to, childrenOnly)

		if this.keyedRemovalList.Len() > 0 {
			for item := this.keyedRemovalList.Front(); item != nil; item = item.Next() {
				key := item.Value.(string)
				if node, found := this.fromNodeLookup[key]; found {
					this.removeNode(node, node.Parent(), false)
				}
			}
		}
	}

	if parent := from.Parent(); !childrenOnly && !node.IsSameNode(from) && parent != nil {
		parent.ReplaceChild(node, from)
	}

	return node
}
