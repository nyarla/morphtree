package morph

import (
	"testing"

	_ "github.com/nyarla/morphtree/backend/golang"
	"github.com/nyarla/morphtree/tree"
)

func TestMorphTree(t *testing.T) {
	target := tree.CreateElement(`root`)
	target.AppendChild(tree.CreateElement(`content`))

	nodeA := tree.CreateElement(`tag`)
	nodeB := tree.CreateElement(`content`)
	nodeC := tree.CreateTextNode(`hello, world!`)

	nodeB.AppendChild(nodeC)
	nodeA.AppendChild(nodeB)
	nodeA.SetAttr(`class`, `foo`)

	instance := New()
	node := instance.Morph(target, nodeA, false)

	if node.FirstChild().FirstChild().TextContent() != `hello, world!` {
		t.Fatalf(`failed to morph tree: %+v`, node)
	}

	if node.GetAttr(`class`).Value() != `foo` {
		t.Fatalf(`failed to morph tree: %+v`, node)
	}
}
