package html

import (
	"bytes"
	"testing"

	_ "github.com/nyarla/morphtree/backend/golang"
	. "github.com/nyarla/morphtree/dsl"
	"github.com/nyarla/morphtree/renderer/html"
	"github.com/nyarla/morphtree/tree"
)

func TestHTMLRenderer(t *testing.T) {
	desc := H(`p`, H(`img`, A{`src`: `#`}), T(`hello, world!`))
	node := desc.Describe(tree.CreateElement(`p`))

	b := new(bytes.Buffer)

	if err := html.Render(b, node); err != nil {
		t.Fatal(err)
	} else {
		if b.String() != `<p><img src="#"/>hello, world!</p>` {
			t.Fatal(`failed to render tree as html.`)
		}
	}
}
