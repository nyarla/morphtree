package html

import (
	"bufio"
	"errors"
	"fmt"
	"html"
	"io"
	"strings"

	"github.com/nyarla/morphtree/tree"
)

var voidElements = map[string]bool{
	"area":    true,
	"base":    true,
	"br":      true,
	"col":     true,
	"command": true,
	"embed":   true,
	"hr":      true,
	"img":     true,
	"input":   true,
	"keygen":  true,
	"link":    true,
	"meta":    true,
	"param":   true,
	"source":  true,
	"track":   true,
	"wbr":     true,
}

var plaintextAbort = errors.New("html: internal error (plaintext abort)")

type writer interface {
	io.Writer
	io.ByteWriter
	WriteString(src string) (int, error)
}

func Render(w io.Writer, root tree.Node) error {
	if x, ok := w.(writer); ok {
		return render(x, root)
	}

	b := bufio.NewWriter(w)
	if err := render(b, root); err != nil {
		return err
	}

	return b.Flush()
}

func escape(w writer, text string) error {
	_, err := w.WriteString(html.EscapeString(text))
	return err
}

func render(w writer, n tree.Node) error {
	err := render1(w, n)
	if err == plaintextAbort {
		err = nil
	}

	return err
}

func render1(w writer, n tree.Node) error {
	switch n.Type() {
	case tree.UnknownNode:
		return errors.New(`html: cannot render an UnknownNode.`)
	case tree.TextNode:
		return escape(w, n.TextContent())
	case tree.CommentNode:
		if _, err := w.WriteString(`<!--`); err != nil {
			return err
		}

		if _, err := w.WriteString(n.TextContent()); err != nil {
			return err
		}

		if _, err := w.WriteString(`-->`); err != nil {
			return err
		}
	case tree.ElementNode:
		// no-op
	case tree.UnsupportedNode:
		return errors.New(`html: cannot render and UnsupportedNode.`)
	default:
		return errors.New(`html: unknown node type.`)
	}

	if err := w.WriteByte('<'); err != nil {
		return err
	}

	if _, err := w.WriteString(n.Name()); err != nil {
		return err
	}

	for _, a := range n.Attrs() {
		if err := w.WriteByte(' '); err != nil {
			return err
		}

		if prefix := a.Prefix(); prefix != `` {
			if _, err := w.WriteString(prefix); err != nil {
				return err
			}

			if err := w.WriteByte(':'); err != nil {
				return err
			}

			if _, err := w.WriteString(a.Name()); err != nil {
				return err
			}
		} else {
			if _, err := w.WriteString(a.Key()); err != nil {
				return err
			}
		}

		if _, err := w.WriteString(`="`); err != nil {
			return err
		}

		if err := escape(w, a.Value()); err != nil {
			return err
		}

		if err := w.WriteByte('"'); err != nil {
			return err
		}
	}

	if voidElements[n.Name()] {
		if n.FirstChild() != nil {
			return fmt.Errorf(`html: void element <%s> has child nodes`, n.Name())
		}

		_, err := w.WriteString(`/>`)
		return err
	}

	if err := w.WriteByte('>'); err != nil {
		return err
	}

	if c := n.FirstChild(); c != nil && c.Type() == tree.TextNode && strings.HasPrefix(c.TextContent(), "\n") {
		switch n.Name() {
		case "pre", "listing", "textarea":
			if err := w.WriteByte('\n'); err != nil {
				return err
			}
		}
	}

	switch n.Name() {
	case "iframe", "noembed", "noframes", "noscript", "plaintext", "script", "style", "xmp":
		for c := n.FirstChild(); c != nil; c = c.NextSibling() {
			if c.Type() == tree.TextNode {
				if _, err := w.WriteString(c.TextContent()); err != nil {
					return err
				}
			} else {
				if err := render1(w, c); err != nil {
					return err
				}
			}
		}
		if n.Name() == `plaintext` {
			return plaintextAbort
		}
	default:
		for c := n.FirstChild(); c != nil; c = c.NextSibling() {
			if err := render1(w, c); err != nil {
				return err
			}
		}
	}

	if _, err := w.WriteString(`</`); err != nil {
		return err
	}

	if _, err := w.WriteString(n.Name()); err != nil {
		return err
	}

	return w.WriteByte('>')
}
