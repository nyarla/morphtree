package tree

type Attribute interface {
	Namespace() string
	Key() string
	Prefix() string
	Name() string
	Value() string
}

type NodeType int64

const (
	UnknownNode NodeType = iota
	TextNode
	CommentNode
	ElementNode
	UnsupportedNode
)

type Node interface {
	Namespace() string
	Prefix() string
	Name() string
	Key() string
	Type() NodeType

	FirstChild() Node
	LastChild() Node
	NextSibling() Node
	PrevSibling() Node
	Parent() Node

	IsSameNode(Node) bool

	RemoveChild(target Node)
	AppendChild(target Node)

	InsertBefore(target Node, ref Node)
	ReplaceChild(target Node, ref Node)

	TextContent() string
	SetTextContent(content string)

	Attrs() []Attribute
	HasAttr(key string) bool
	GetAttr(key string) Attribute
	RemoveAttr(key string)
	SetAttr(key, val string)

	HasAttrNS(namespace, key string) bool
	GetAttrNS(namespace, key string) Attribute
	RemoveAttrNS(namespace, key string)
	SetAttrNS(namespace, key, val string)

	On(name string, fn func(target Node) Node)
	Off(name string)
	Trigger(name string)
}

type Backend interface {
	CreateTextNode(content string) Node
	CreateComment(msg string) Node
	CreateElement(tag string) Node
	CreateElementNS(nsURI string, tag string) Node
	CreateAttribute(key, val string) Attribute
	CreateAttributeNS(nsURI string, key, val string) Attribute
}
