package tree

var _backend Backend = nil

func RegisterBackend(backend Backend) {
	_backend = backend
}

func CreateTextNode(content string) Node {
	if _backend != nil {
		return _backend.CreateTextNode(content)
	}

	panic(`no backend found.`)
}

func CreateComment(content string) Node {
	if _backend != nil {
		return _backend.CreateComment(content)
	}

	panic(`no backend found.`)
}
func CreateElement(tag string) Node {
	if _backend != nil {
		return _backend.CreateElement(tag)
	}

	panic(`no backend found.`)
}

func CreateElementNS(nsURI string, tag string) Node {
	if _backend != nil {
		return _backend.CreateElementNS(nsURI, tag)
	}

	panic(`no backend found.`)
}

func CreateAttribute(key, val string) Attribute {
	if _backend != nil {
		return _backend.CreateAttribute(key, val)
	}

	panic(`no backend found.`)
}

func CreateAttributeNS(nsURI, key, val string) Attribute {
	if _backend != nil {
		return _backend.CreateAttributeNS(nsURI, key, val)
	}

	panic(`no backend found.`)
}
